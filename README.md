# agenndy - Minimalist text-based activity log with CSV backend

agenndy is a minimal, zero-dependency text-based activity log (or personal
agenda).

It takes a text file which follows some very basic (but strict) rules and
turns it into a CSV file (which includes times, activities and hours spent
for each activity) suited for further processing.

Its intended purpose is to be used by remote or self-employed workers tired of
editing huge spreadsheets.

Its name, of course, is a tribute to animator Genndy Tartakovsky.

## Usage

Create a `YYYY-MM.txt` file. You are then meant to edit the created
file by hand, that's why it's text-based ;)

agenndy supports text files with this schema:

```
# Filename: 2033-05.txt
[1]
08:00-09:00 Breakfast
09:00-10:00 Another action

# Anything which is not a date header or an entry gets ignored, btw :)
[2]
08:00-13:00 Hard work
```

One activity per line. Activities always start with a `hh:mm-hh:mm` timespan
followed by a whitespace and then the name of the activity.

To convert this text file in the agenndy format to a CSV file, use
`agenndy FILE` (in the case of the example, `agenndy 2033-05.txt`).

agenndy is pretty untolerant of mistakes to help you waste less time writing
fancy textfiles. So please stick to the schema...or use org-mode :D

## Acknowledgements

Thanks to Tsuyoshi Sawada (sawa), whose StackOverflow post lead me to the right
version of the split + positive lookahead trick I wanted to use.

## License

Everything in this repository is released under the
GNU Affero General Public License, version 3 or any later version, unless
otherwise specified (and even in that case, it's all free software).
