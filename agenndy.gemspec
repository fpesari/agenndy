# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

Gem::Specification.new do |s|
  s.name          = 'agenndy'
  s.version       = '0.1.1'
  s.summary       = 'Text-based activity log with hour count and CSV output'
  s.description   = <<~DESC
    agenndy is a minimal text-based activity log (or personal agenda).

    It takes a text file which follows some very basic (but strict) rules and
    turns it into a CSV file (which includes times, activities and hours spent
    for each activity) suited for further processing.

    For the schema of the text-based agenda, check out the examples/ directory.
  DESC
  s.authors       = ['Fabio Pesari']
  s.homepage      = 'https://gitlab.com/fpesari/agenndy'
  s.files         = %w[.rubocop.yml agenndy.gemspec
                       LICENSE bin/agenndy examples/2033-05.txt README.md]
  s.executables   = %w[agenndy]
  s.license       = 'AGPL-3.0+'
  s.required_ruby_version = '>= 2.5.0'
end
